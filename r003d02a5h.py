from functools import reduce
import talib.abstract as ta
from pandas import DataFrame
from technical import qtpylib
import math
from datetime import datetime, timedelta
from freqtrade.persistence import Trade
from freqtrade.strategy import CategoricalParameter, IStrategy, merge_informative_pair
import logging
import numpy as np
import pandas as pd
import talib.abstract as ta
from pandas import DataFrame, Series, DatetimeIndex, merge
from technical import qtpylib

from freqtrade.strategy import IntParameter, IStrategy, merge_informative_pair


logger = logging.getLogger(__name__)


class r003d02a5h(IStrategy):

    minimal_roi = {
        "0": 0.003
    }

    # Optimal stoploss designed for the strategy
    stoploss = -0.99

    use_custom_stoploss = False
    process_only_new_candles = True
    startup_candle_count: int = 100
    can_short = False

    # Hyperoptable parameters
    buy_rsi = IntParameter(low=1, high=50, default=32, space='buy', optimize=True, load=True)
    # sell_rsi = IntParameter(low=50, high=100, default=70, space='sell', optimize=True, load=True)
    # short_rsi = IntParameter(low=51, high=100, default=70, space='sell', optimize=True, load=True)
    # exit_short_rsi = IntParameter(low=1, high=50, default=30, space='buy', optimize=True, load=True)

    # FreqAI required function, leave as is or add additional informatives to existing structure.
    def informative_pairs(self):
        whitelist_pairs = self.dp.current_whitelist()
        corr_pairs = self.config["freqai"]["feature_parameters"]["include_corr_pairlist"]
        informative_pairs = []
        for tf in self.config["freqai"]["feature_parameters"]["include_timeframes"]:
            for pair in whitelist_pairs:
                informative_pairs.append((pair, tf))
            for pair in corr_pairs:
                if pair in whitelist_pairs:
                    continue  # avoid duplication
                informative_pairs.append((pair, tf))
        return informative_pairs

    # FreqAI required function, user can add or remove indicators, but general structure
    # must stay the same.
    def populate_any_indicators(
            self, pair, df, tf, informative=None, set_generalized_indicators=False
    ):
        """
        User feeds these indicators to FreqAI to train a classifier to decide
        if the market will go up or down.

        :param pair: pair to be used as informative
        :param df: strategy dataframe which will receive merges from informatives
        :param tf: timeframe of the dataframe which will modify the feature names
        :param informative: the dataframe associated with the informative pair
        """

        coin = pair.split('/')[0]

        if informative is None:
            informative = self.dp.get_pair_dataframe(pair, tf)

        # first loop is automatically duplicating indicators for time periods
        for t in self.freqai_info["feature_parameters"]["indicator_periods_candles"]:

            t = int(t)
            informative[f"%-{coin}rsi-period_{t}"] = ta.RSI(informative, timeperiod=t)
            # informative[f"%-{coin}mfi-period_{t}"] = ta.MFI(informative, timeperiod=t)
            # informative[f"%-{coin}adx-period_{t}"] = ta.ADX(informative, window=t)
            # informative[f"%-{coin}sma-period_{t}"] = ta.SMA(informative, timeperiod=t)
            # informative[f"%-{coin}ema-period_{t}"] = ta.EMA(informative, timeperiod=t)
            # informative[f"%-{coin}roc-period_{t}"] = ta.ROC(informative, timeperiod=t)
            informative[f"%-{coin}relative_volume-period_{t}"] = (
                    informative["volume"] / informative["volume"].rolling(t).mean()
            )

        # FreqAI needs the following lines in order to detect features and automatically
        # expand upon them.
        indicators = [col for col in informative if col.startswith("%")]
        # This loop duplicates and shifts all indicators to add a sense of recency to data
        for n in range(self.freqai_info["feature_parameters"]["include_shifted_candles"] + 1):
            if n == 0:
                continue
            informative_shift = informative[indicators].shift(n)
            informative_shift = informative_shift.add_suffix("_shift-" + str(n))
            informative = pd.concat((informative, informative_shift), axis=1)

        df = merge_informative_pair(df, informative, self.config["timeframe"], tf, ffill=True)
        skip_columns = [
            (s + "_" + tf) for s in ["date", "open", "high", "low", "close", "volume"]
        ]
        df = df.drop(columns=skip_columns)

        # User can set the "target" here (in present case it is the
        # "up" or "down")
        if set_generalized_indicators:
            # User "looks into the future" here to figure out if the future
            # will be "up" or "down". This same column name is available to
            # the user
            df['&s-up_or_down'] = np.where(df["close"].shift(-50) >
                                           df["close"], 'up', 'down')

        return df

    # flake8: noqa: C901
    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:

        # User creates their own custom strat here. Present example is a supertrend
        # based strategy.

        dataframe = self.freqai.start(dataframe, metadata, self)

        # TA indicators to combine with the Freqai targets
        # RSI
        # dataframe['rsi'] = ta.RSI(dataframe)
        rsi = ta.RSI(dataframe)
        dataframe["rsi"] = rsi
        # rsi = 0.1 * (rsi - 50)
        # dataframe["fisher"] = (np.exp(2 * rsi) - 1) / (np.exp(2 * rsi) + 1)
        # Bollinger Bands
        # bollinger = qtpylib.bollinger_bands(qtpylib.typical_price(dataframe), window=20, stds=2)
        # dataframe['bb_lowerband'] = bollinger['lower']
        # dataframe['bb_middleband'] = bollinger['mid']
        # dataframe['bb_upperband'] = bollinger['upper']
        # dataframe["bb_percent"] = (
        #         (dataframe["close"] - dataframe["bb_lowerband"]) /
        #         (dataframe["bb_upperband"] - dataframe["bb_lowerband"])
        # )
        # dataframe["bb_width"] = (
        #         (dataframe["bb_upperband"] - dataframe["bb_lowerband"]) / dataframe["bb_middleband"]
        # )

        # TEMA - Triple Exponential Moving Average
        # dataframe['tema'] = ta.TEMA(dataframe, timeperiod=9)

        return dataframe

    def populate_entry_trend(self, df: DataFrame, metadata: dict) -> DataFrame:

        df.loc[
            (
                # Signal: RSI crosses above 30
                    # (qtpylib.crossed_above(dataframe['rsi'], 30)) &  # Signal: RSI crosses above 30
                    (df['rsi'] < self.buy_rsi.value) &
                    # (qtpylib.crossed_above(dataframe['rsi'], 32)) &
                    # (dataframe['tema'] > dataframe['tema'].shift(1)) &
                    (df['volume'] > 0) &
                    # (qtpylib.crossed_above(df['rsi'], self.buy_rsi.value)) &
                    # (df['tema'] <= df['bb_middleband']) &  # Guard: tema below BB middle
                    # (df['tema'] > df['tema'].shift(1)) &  # Guard: tema is raising
                    # (df['volume'] > 0) &  # Make sure Volume is not 0
                    (df['do_predict'] == 1) &  # Make sure Freqai is confident in the prediction
                    # Only enter trade if Freqai thinks the trend is in this direction
                    (df['&s-up_or_down'] == 'up')
            ),
            'enter_long'] = 1

        # df.loc[
        #     (
        #         # Signal: RSI crosses above 70
        #             (qtpylib.crossed_above(df['rsi'], self.short_rsi.value)) &
        #             (df['tema'] > df['bb_middleband']) &  # Guard: tema above BB middle
        #             (df['tema'] < df['tema'].shift(1)) &  # Guard: tema is falling
        #             (df['volume'] > 0) &  # Make sure Volume is not 0
        #             (df['do_predict'] == 1) &  # Make sure Freqai is confident in the prediction
        #             # Only enter trade if Freqai thinks the trend is in this direction
        #             (df['&s-up_or_down'] == 'down')
        #     ),
        #     'enter_short'] = 1

        return df

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:

        dataframe.loc[
            ()
            ,'sell'] = 1

        return dataframe

    position_adjustment_enable = True

    initial_safety_order_trigger = -0.02  #2% 0.02 #1.5% it's 0.5%
    max_safety_orders = 5                  #
    safety_order_step_scale = 0.5         #SS
    safety_order_volume_scale = 1.6       #OS

    # Auto compound calculation
    max_dca_multiplier = (1 + max_safety_orders)
    if (max_safety_orders > 0):
        if (safety_order_volume_scale > 1):
            max_dca_multiplier = (2 + (safety_order_volume_scale * (math.pow(safety_order_volume_scale, (max_safety_orders - 1)) - 1) / (safety_order_volume_scale - 1)))
        elif (safety_order_volume_scale < 1):
            max_dca_multiplier = (2 + (safety_order_volume_scale * (1 - math.pow(safety_order_volume_scale, (max_safety_orders - 1))) / (1 - safety_order_volume_scale)))

    # Let unlimited stakes leave funds open for DCA orders
    def custom_stake_amount(self, pair: str, current_time: datetime, current_rate: float,
                            proposed_stake: float, min_stake: float, max_stake: float,
                            **kwargs) -> float:

        if self.config['stake_amount'] == 'unlimited':
            return proposed_stake / self.max_dca_multiplier

        return proposed_stake

    # DCA
    def adjust_trade_position(self, trade: Trade, current_time: datetime,
                              current_rate: float, current_profit: float, min_stake: float,
                              max_stake: float, **kwargs):

        if current_profit > self.initial_safety_order_trigger:
            return None

        count_of_buys = trade.nr_of_successful_buys

        if 1 <= count_of_buys <= self.max_safety_orders:
            safety_order_trigger = (abs(self.initial_safety_order_trigger) * count_of_buys)
            if (self.safety_order_step_scale > 1):
                safety_order_trigger = abs(self.initial_safety_order_trigger) + (abs(self.initial_safety_order_trigger) * self.safety_order_step_scale * (math.pow(self.safety_order_step_scale,(count_of_buys - 1)) - 1) / (self.safety_order_step_scale - 1))
            elif (self.safety_order_step_scale < 1):
                safety_order_trigger = abs(self.initial_safety_order_trigger) + (abs(self.initial_safety_order_trigger) * self.safety_order_step_scale * (1 - math.pow(self.safety_order_step_scale,(count_of_buys - 1))) / (1 - self.safety_order_step_scale))

            if current_profit <= (-1 * abs(safety_order_trigger)):
                try:
                    stake_amount = self.wallets.get_trade_stake_amount(trade.pair, None)
                    # This calculates base order size
                    stake_amount = stake_amount / self.max_dca_multiplier
                    # This then calculates current safety order size
                    stake_amount = stake_amount * math.pow(self.safety_order_volume_scale, (count_of_buys - 1))
                    amount = stake_amount / current_rate
                    logger.info(f"Initiating safety order buy #{count_of_buys} for {trade.pair} with stake amount of {stake_amount} which equals {amount}")
                    return stake_amount
                except Exception as exception:
                    logger.info(f'Error occured while trying to get stake amount for {trade.pair}: {str(exception)}')
                    return None

        return None
# Williams %R
# def williams_r(dataframe: DataFrame, period: int = 14) -> Series:
#     """Williams %R, or just %R, is a technical analysis oscillator showing the current closing price in relation to the high and low
#         of the past N days (for a given N). It was developed by a publisher and promoter of trading materials, Larry Williams.
#         Its purpose is to tell whether a stock or commodity market is trading near the high or the low, or somewhere in between,
#         of its recent trading range.
#         The oscillator is on a negative scale, from −100 (lowest) up to 0 (highest).
#     """
#
#     highest_high = dataframe["high"].rolling(center=False, window=period).max()
#     lowest_low = dataframe["low"].rolling(center=False, window=period).min()
#
#     WR = Series(
#         (highest_high - dataframe["close"]) / (highest_high - lowest_low),
#         name=f"{period} Williams %R",
#         )
#
#     return WR * -100
    # def populate_exit_trend(self, df: DataFrame, metadata: dict) -> DataFrame:
    #
    #     df.loc[
    #         (
    #             # Signal: RSI crosses above 70
    #                 (qtpylib.crossed_above(df['rsi'], self.sell_rsi.value)) &
    #                 (df['tema'] > df['bb_middleband']) &  # Guard: tema above BB middle
    #                 (df['tema'] < df['tema'].shift(1)) &  # Guard: tema is falling
    #                 (df['volume'] > 0)  # Make sure Volume is not 0
    #         ),
    #
    #         'exit_long'] = 1
    #
    #     df.loc[
    #         (
    #             # Signal: RSI crosses above 30
    #                 (qtpylib.crossed_above(df['rsi'], self.exit_short_rsi.value)) &
    #                 # Guard: tema below BB middle
    #                 (df['tema'] <= df['bb_middleband']) &
    #                 (df['tema'] > df['tema'].shift(1)) &  # Guard: tema is raising
    #                 (df['volume'] > 0)  # Make sure Volume is not 0
    #         ),
    #         'exit_short'] = 1
    #
    #     return df
